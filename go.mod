module gitlab.com/TeskTaskCourse/test-task-backend

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/rs/cors v1.7.0
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
