package todos

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/TeskTaskCourse/test-task-backend/backand/domain/todos"
	"gitlab.com/TeskTaskCourse/test-task-backend/backand/services"
	"gitlab.com/TeskTaskCourse/test-task-backend/backand/utils/errors"
	"gitlab.com/TeskTaskCourse/test-task-backend/backand/utils/date_utils"
)

//GetTodos is ...
func GetTodos(c *gin.Context){
	todos, err := serveces.GetTodos()
	if err != nil {
		c.JSON(err.Status, err)
	}
	c.JSON(http.StatusOK, todos)
}


//GetTodoByID is ...
func GetTodoByID(c *gin.Context){
	todoID, convErr := strconv.ParseInt(c.Param("todo_id"), 10, 64)
	if convErr != nil {
		err := errors.NewBadRequestError("todo id should be a number")
		c.JSON(err.Status, err)
		return
	}
	todo, err := serveces.GetTodoByID(todoID)
	if err != nil {
		c.JSON(err.Status, err)
	}
	c.JSON(http.StatusOK, todo)
}

//CreateTodo is...
func CreateTodo(c *gin.Context){
	var todo todos.Todo
	if err := c.ShouldBindJSON(&todo); err != nil {
		restErr := errors.NewBadRequestError("invalid json body")
		c.JSON(restErr.Status, restErr)
		return
	}
	err := date_utils.ParseTime(todo.DueDate)
	if err != nil {
		c.JSON(err.Status, err)
	}

	res, err := serveces.CreateTodo(&todo)
	if err != nil {
		c.JSON(err.Status, err)
	}
	c.JSON(http.StatusCreated, res)
}

//UpdateTodo is...
func UpdateTodo(c *gin.Context){
	var todo todos.Todo
	if err := c.ShouldBindJSON(&todo); err != nil {
		restErr := errors.NewBadRequestError("invalid json body")
		c.JSON(restErr.Status, restErr)
		return
	}
	errDate := date_utils.ParseTime(todo.DueDate)
	if errDate != nil {
		c.JSON(errDate.Status, errDate)
	}
	err := serveces.UpdateTodo(&todo)
	if err != nil {
		c.JSON(err.Status, err)
	}
	c.JSON(http.StatusOK, fmt.Sprintf("todo %d succefully updated", todo.ID))
}

//DeleteTodo is...
func DeleteTodo(c *gin.Context){
	todoID, convErr := strconv.ParseInt(c.Param("todo_id"), 10, 64)
	if convErr != nil {
		err := errors.NewBadRequestError("todo id should be a number")
		c.JSON(err.Status, err)
		return
	}
	err := serveces.DeleteTodo(todoID)
	if err != nil {
		c.JSON(err.Status, err)
	}
	c.JSON(http.StatusOK, fmt.Sprintf("todo %d succefully deleted", todoID))
}