package db


import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"os"
)

const (
	
	mysqlTodosUsername = "mysql_todos_username"
	mysqlTodosPassword = "mysql_todos_password"
	mysqlTodosHost     = "mysql_todos_host"
	mysqlTodosSchema   = "mysql_todos_schema"
)

var (
	client *sql.DB

	username = os.Getenv(mysqlTodosUsername)
	password = os.Getenv(mysqlTodosPassword)
	host     = os.Getenv(mysqlTodosHost)
	schema   = os.Getenv(mysqlTodosSchema)
)

//ConnDB is ...
func ConnDB() *sql.DB{
	dataSourceName := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8",
		username, password, host, schema,
	)
	var err error
	client, err = sql.Open("mysql", dataSourceName)
	if err != nil {
		panic(err)
	}
	if err = client.Ping(); err != nil {
		panic(err)
	}
	log.Println("database successfully configured")
	return client

}



