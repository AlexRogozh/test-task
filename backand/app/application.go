package app

import (
	"github.com/gin-gonic/gin"
	cors "github.com/rs/cors/wrapper/gin"
)

var (
	router = gin.Default()
)

//StartApplication is...
func StartApplication() {
	router.Use(cors.Default())
	mapUrls()
	router.Run(":8081")
}
