package app

import (
	"gitlab.com/TeskTaskCourse/test-task-backend/backand/controllers/todos"
	"gitlab.com/TeskTaskCourse/test-task-backend/backand/controllers/ping"
)

func mapUrls() {
	router.GET("/ping", ping.Ping)

	router.GET("/todos", todos.GetTodos)
	router.GET("/todos/:id", todos.GetTodoByID)
	router.POST("/todos", todos.CreateTodo)
	router.PUT("/todos/:id", todos.UpdateTodo)
	router.DELETE("/todos/:id", todos.DeleteTodo)
}
