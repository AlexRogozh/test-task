package date_utils

import (
	"time"

	"gitlab.com/TeskTaskCourse/test-task-backend/backand/utils/errors"
)

const (
	apiDateLayout = "2006-01-02"
)

//ParseTime is...
func ParseTime(date string) *errors.RestErr {
	_, err := time.Parse(apiDateLayout, date)
	if err != nil {
		return errors.NewBadRequestError("incorrect format for date")
	}
	return nil
}
