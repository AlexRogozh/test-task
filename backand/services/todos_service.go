package serveces

import (
	"gitlab.com/TeskTaskCourse/test-task-backend/backand/domain/todos"
	"gitlab.com/TeskTaskCourse/test-task-backend/backand/utils/errors"
)

//GetTodos is...
func GetTodos() ([]*todos.Todo, *errors.RestErr) {
	td := todos.Todo{}
	todos, err := td.GetAllTodos()
	if err != nil {
		return nil, err
	}
	return todos, nil
}

//GetTodoByID is...
func GetTodoByID(id int64) (*todos.Todo, *errors.RestErr) {
	todo := &todos.Todo{
		ID: id,
	}
	err := todo.GetTodo()
	if err != nil {
		return nil, err
	}
	return todo, nil

}

//CreateTodo is ...
func CreateTodo(t *todos.Todo) (*todos.Todo, *errors.RestErr) {
	if err := t.Validate(); err != nil {
		return nil, err
	}
	if err := t.CreateTodo(); err != nil {
		return nil, err
	}
	return t, nil

}

//UpdateTodo is ...
func UpdateTodo(t *todos.Todo) *errors.RestErr {
	if err := t.UpdateTodo(); err != nil {
		return err
	}
	return nil
}

//DeleteTodo is...
func DeleteTodo(id int64) *errors.RestErr {
	todo := &todos.Todo{
		ID: id,
	}
	if err := todo.DeleteTodo(); err != nil {
		return err
	}
	return nil
}
