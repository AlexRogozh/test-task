package todos

import (
	"fmt"

	"gitlab.com/TeskTaskCourse/test-task-backend/backand/datasourses/mysql/db"
	"gitlab.com/TeskTaskCourse/test-task-backend/backand/utils/errors"
)

const (
	queryGetAllTodos = "SELECT * FROM todos;"
	queryGetTodo = "SELECT * FROM todos WHERE id=?;"
	queryInsertTodo  = "INSERT INTO todos(title, complited, duedate) VALUES(?, ?, ?);"
	queryUpdateTodo = "UPDATE todos SET title=?, complited=?, duedate=? WHERE id=?;"
	queryDeleteTodo = "DELETE * FROM todos WHERE id=?;"
)

//GetAllTodos is ...
func (t *Todo) GetAllTodos() ([]*Todo, *errors.RestErr) {
	conn := db.ConnDB()
	defer conn.Close()
	selDB, err := conn.Query(queryGetAllTodos)
	if err != nil {
		return nil, errors.NewInternalServerError("fail query to db")
	}

	todos := []*Todo{}

	for selDB.Next() {
		todo := &Todo{}
		if err := selDB.Scan(&todo.ID, &todo.Title, &todo.Complited, &todo.DueDate); err != nil {
			return nil, errors.NewInternalServerError("error when scan db rows")
		}
		todos = append(todos, todo)
	}

	return todos, nil
}

//GetTodo is ...
func (t *Todo) GetTodo() *errors.RestErr {
	conn := db.ConnDB()
	defer conn.Close()
	selDB, err := conn.Query(queryGetTodo, t.ID)
	if err != nil {
		return  errors.NewInternalServerError("fail query to db")
	}
	for selDB.Next() {
		if err := selDB.Scan(&t.ID, &t.Title, &t.Complited, &t.DueDate); err != nil {
			return  errors.NewInternalServerError("error when scan db rows")
		}

	}
	return nil
}

//CreateTodo is...
func(t *Todo) CreateTodo() *errors.RestErr {
	conn := db.ConnDB()
	defer conn.Close()
	stmt, err := conn.Prepare(queryInsertTodo)
	if err != nil {
		stmt.Close()
		return errors.NewInternalServerError(err.Error())
	}
	insertRes, err := stmt.Exec(t.Title, t.Complited, t.DueDate)
	if err != nil {
		stmt.Close()
		return errors.NewInternalServerError(fmt.Sprintf("error when trying save todo into database: %s", err.Error()))
	}
	insID, err := insertRes.LastInsertId()
	if err != nil {
		return errors.NewInternalServerError(fmt.Sprintf("error when trying save todo into database: %s", err.Error()))
	}
	t.ID = insID
	return nil
}

//UpdateTodo is...
func (t *Todo) UpdateTodo() *errors.RestErr {
	conn := db.ConnDB()
	defer conn.Close()
	stmt, err := conn.Prepare(queryInsertTodo)
	if err != nil {
		stmt.Close()
		return errors.NewInternalServerError(err.Error())
	}
	_ , err = stmt.Exec(t.Title, t.Complited, t.DueDate, t.ID)
	if err != nil {
		stmt.Close()
		return errors.NewInternalServerError(fmt.Sprintf("error when trying update todo: %s", err.Error()))
	}
	return nil	
}

//DeleteTodo is...
func (t *Todo) DeleteTodo() *errors.RestErr {
	conn := db.ConnDB()
	defer conn.Close()
	stmt, err := conn.Prepare(queryDeleteTodo)
	if err != nil {
		stmt.Close()
		return errors.NewInternalServerError(err.Error())
	}
	_ , err = stmt.Exec(t.ID)
	if err != nil {
		stmt.Close()
		return errors.NewInternalServerError(fmt.Sprintf("error when trying delete todo: %s", err.Error()))
	}
	return nil	
}