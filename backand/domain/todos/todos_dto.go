package todos

import (
	"gitlab.com/TeskTaskCourse/test-task-backend/backand/utils/errors"
)

//Todo is ...
type Todo struct {
	ID        int64  `json:"id"`
	Title     string `json:"title"`
	Complited bool   `json:"complited"`
	DueDate   string `json:"dueTime"`
}

//Validate is...
func (t *Todo) Validate() *errors.RestErr {
	if t.Title == "" {
		return errors.NewBadRequestError("empty title")
	}
	return nil
}
